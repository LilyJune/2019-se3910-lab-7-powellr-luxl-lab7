var class_p_w_m_manager =
[
    [ "GPIOPWMControlMapStruct", "struct_p_w_m_manager_1_1_g_p_i_o_p_w_m_control_map_struct.html", "struct_p_w_m_manager_1_1_g_p_i_o_p_w_m_control_map_struct" ],
    [ "PWMManager", "class_p_w_m_manager.html#a908865cec966c4f8f31eb5341edb8c74", null ],
    [ "~PWMManager", "class_p_w_m_manager.html#a9573ebd6cee2a7c1c36abcdb2dd32bd1", null ],
    [ "addPWMPin", "class_p_w_m_manager.html#af5bfadf62a2a9dc63a2461ef1b45c2ba", null ],
    [ "removePWMPin", "class_p_w_m_manager.html#a8bb4c74b1cc86f6042cc591c792fdcec", null ],
    [ "setDutyCycle", "class_p_w_m_manager.html#ac22c84de78a0aea27631c959d99cdf12", null ],
    [ "taskMethod", "class_p_w_m_manager.html#a4468848bc8b0a3fab9f9b24496846572", null ],
    [ "currentCount", "class_p_w_m_manager.html#afbb50d7818b888cd3ca191a8726a5404", null ],
    [ "GPIOControlMap", "class_p_w_m_manager.html#ad7aad44bc5ed6bddaa104603b824217a", null ]
];