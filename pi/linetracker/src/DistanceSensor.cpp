/*
 * DistanceSensor.cpp
 *
 *  Created on: Oct 20, 2019
 *      Author: se3910
 */

#include "DistanceSensor.h"
#include <unistd.h>
#include <iostream>

namespace se3910RPiHCSR04 {

DistanceSensor::DistanceSensor(int trigPin, int echoPin, std::string threadName, uint32_t period): PeriodicTask(threadName, period) {
	this->tPin = new se3910RPi::GPIO(trigPin, se3910RPi::GPIO::GPIO_OUT);
//	this->tPin->enableEdgeInterrupt(se3910RPi::GPIO::GPIO_BOTH);
	this->ePin = new se3910RPi::GPIO(echoPin, se3910RPi::GPIO::GPIO_IN);
	this->ePin->enableEdgeInterrupt(se3910RPi::GPIO::GPIO_BOTH);
}

DistanceSensor::~DistanceSensor() {
	delete this->tPin;
	delete this->ePin;
}

void DistanceSensor::taskMethod() {
	int edgeReturnRising, edgeReturnFalling;
	timespec risingStamp, fallingStamp;

//	this->tPin->enableEdgeInterrupt(se3910RPi::GPIO::GPIO_BOTH);
//	this->ePin->enableEdgeInterrupt(se3910RPi::GPIO::GPIO_BOTH);

	this->tPin->setValue(se3910RPi::GPIO::GPIO_HIGH);
	usleep(10);
	this->tPin->setValue(se3910RPi::GPIO::GPIO_LOW);

	edgeReturnRising = this->ePin->waitForEdge(5);
//	std::cout << edgeReturnRising << std::endl;
	if (edgeReturnRising == 0) {
//		std::cout << "recieved edge rising" << std::endl;
		risingStamp = this->ePin->getRisingISRTimestamp();
	}


	edgeReturnFalling = this->ePin->waitForEdge(50);
//	std::cout << edgeReturnFalling << std::endl;
	if (edgeReturnFalling == 0) {
//		std::cout << "recieved edge falling";
		fallingStamp = this->ePin->getFallingISRTimestamp();
	}

	if(edgeReturnRising == 0 && edgeReturnFalling == 0) {
		int delta = (fallingStamp.tv_nsec - risingStamp.tv_nsec);

//		std::cout << "time: " << delta << std::endl;
		this->currentDistance = ((delta / 2) * SPEED_OF_SOUND);

//		std::cout << "distance: " << this->currentDistance << std::endl;
		if(this->currentDistance >= 0 && this->currentDistance <= 2500) {
			this->distanceRecordingCount++;
			this->totalOfAllDistances += this->currentDistance;
			if(this->currentDistance <= this->minDistance) {
				this->minDistance = this->currentDistance;
			}
			if(this->currentDistance >= this->maxDistance) {
				this->maxDistance = this->currentDistance;
			}
		}
	}
}

int DistanceSensor::getMaxDistance() {
	return this->maxDistance;
}

int DistanceSensor::getMinDistance() {
	return this->minDistance;
}

int DistanceSensor::getCurrentDistance() {
	return this->currentDistance;
}

int DistanceSensor::getAverageDistance() {
	int retVal = 0;
	if (this->distanceRecordingCount > 0) {
	  retVal = this->totalOfAllDistances / this->distanceRecordingCount;
	}
	return retVal;
}

void DistanceSensor::resetDistanceRanges() {
	this->minDistance = 2500;
	this->maxDistance = 0;
}

} /* namespace se3910RPiHCSR04 */
