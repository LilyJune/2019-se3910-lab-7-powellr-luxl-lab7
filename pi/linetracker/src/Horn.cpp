/*
 * Horn.cpp
 *
 *  Created on: Oct 23, 2019
 *      Author: se3910
 */

#include <unistd.h>
#include "Horn.h"
#include "TaskRates.h"

Horn::Horn(int gpioPin, std::string threadName, uint32_t taskRate) : PeriodicTask(threadName, taskRate) {
	this->hornPin = new se3910RPi::GPIO(gpioPin, se3910RPi::GPIO::GPIO_OUT);
}

Horn::~Horn() {
	delete hornPin;
}

void Horn::soundHorn() {
	this->hornCount = 0;
}

void Horn::pulseHorn(int length, int period) {
	this->hornCount = 1;
	this->length = length;
	this->repetitionTime = period;
}

void Horn::silenceHorn() {
	this->hornCount = -1;
}

void Horn::taskMethod() {
	if(this->hornCount < 0) {
		this->hornPin->setValue(se3910RPi::GPIO::VALUE::GPIO_LOW);
	} else if (this->hornCount == 0 ) {
		this->hornPin->setValue(se3910RPi::GPIO::VALUE::GPIO_HIGH);
	} else if (this->hornCount > 0) {
		if (this->hornCount < this->length) {
			this->hornPin->setValue(se3910RPi::GPIO::VALUE::GPIO_HIGH);
			this->hornCount = (this->hornCount % this->repetitionTime) + 1;
		} else {
			this->hornPin->setValue(se3910RPi::GPIO::VALUE::GPIO_LOW);
			this->hornCount = (this->hornCount % this->repetitionTime) + 1;
		}
	}

}
