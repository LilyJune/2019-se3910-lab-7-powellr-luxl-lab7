/**
 * @file PWMManager.h
 * @author  Walter Schilling (schilling@msoe.edu)
 * @version 1.0
 *
 * @section LICENSE
 *
 *
 * This code is developed as part of the MSOE SE3910 Real Time Systems course,
 * but can be freely used by others.
 *
 * SE3910 Real Time Systems is a required course for students studying the
 * discipline of software engineering.
 *
 * This Software is provided under the License on an "AS IS" basis and
 * without warranties of any kind concerning the Software, including
 * without limitation merchantability, fitness for a particular purpose,
 * absence of defects or errors, accuracy, and non-infringement of
 * intellectual property rights other than copyright. This disclaimer
 * of warranty is an essential part of the License and a condition for
 * the grant of any rights to this Software.
 *
 * @section DESCRIPTION
 * This file implements the PWM manager, which allows GPIO pins to be speed controlled using PWM.
 */
#include "PWMManager.h"

	/**
	 * This method will instantiate a new PWM controller.
	 * @param threadName This si the name of the thread.
	 * @param period  This is the period task rate for the task, given in microseconds.
	 */
PWMManager::PWMManager(std::string threadName, uint32_t period) :
		PeriodicTask(threadName, period) {
	currentCount = 0;
}

/**
 * This is the destructor that will destroy the given instance of the PWM manager.
 */
PWMManager::~PWMManager() {

}

/**
 * This method will add a given GPIO pin to the PWM controller, making the pin turn on and off when the times expire.
 * @param gpioNumber This si the GPIO pin number.
 * @param instance This is the GPIO instance associated with the pin.
 * @param dutyCycle This si the duty cycle for the given pin.
 */
void PWMManager::addPWMPin(int gpioNumber, se3910RPi::GPIO* instance,
		uint32_t dutyCycle) {
	if (gpioNumber < NUMBER_OF_GPIO_PINS && instance != NULL && dutyCycle >= 0
			&& dutyCycle <= 100) {
		GPIOPWMControlMapStruct temp;
		temp.dutyCycle = 0;
		temp.newDutyCycle = dutyCycle;
		temp.gpioPin = instance;
		temp.changed = true;

		GPIOControlMap[gpioNumber] = temp;
	}
}

/**
 * This method will remove a given GPIO pin from being controlled via PWM.
 * @param gpioNumber This is the GPIO pin that is no longer to be PWM controlled.
 */
void PWMManager::removePWMPin(int gpioNumber) {
	if (gpioNumber < NUMBER_OF_GPIO_PINS) {
		// TODO FInish this	}
	}
}

/**
 * This method will adjust the duty cycle for the given GPIO pin.
 * @param gpioNumber This is the pin number.
 * @param dutyCycle This is the new duty cycle.
 */
void PWMManager::setDutyCycle(int gpioNumber, uint32_t dutyCycle) {
	if (gpioNumber < NUMBER_OF_GPIO_PINS && dutyCycle >= 0 && dutyCycle <= 100) {
		auto search = GPIOControlMap.find(gpioNumber);

		if (search != GPIOControlMap.end()) {
			search->second.newDutyCycle = dutyCycle/4;
			search->second.changed = true;
		}
	}
}

/**
 * This is the task method that will periodically be invoked.
 */
void PWMManager::taskMethod() {
	/**
	 * 1. If the current count is 0, which is where everything starts at.
	 */
	if (currentCount == 0) {
		/**
		 * 1.1 Obtain an iterator from the map.
		 */
		std::map<int, GPIOPWMControlMapStruct>::iterator it = GPIOControlMap.begin();
		/**
		 * 1.1.1 Iterate over all of the items in the map.
		 */
		while (it != GPIOControlMap.end()) {
			/**
			 * 1.1.1.1 Determine if the DC is changed based upon the changed flag being set.  If it is set, update the duty cycle and clear the changed flag.
			 */
			if (it->second.changed==true)
			{
				/**
				 * 1.1.1.1.a Reset the changed flag to false.
				 */
				it->second.changed = false;

				/**
				 * 1.1.1.1.b Up[date the duty cycle to the new duty cycle.
				 */
				it->second.dutyCycle = it->second.newDutyCycle;
			}

			/**
			 * 1.1.1.2 If the duty cycle is greater than 0, turn the pin on.
			 */
			if (it->second.dutyCycle > 0) {
				it->second.gpioPin->setValue(se3910RPi::GPIO::GPIO_HIGH);
			}
			/**
			 * Increment the iterator.
			 */
			it++;
		}
		/**
		 * 2. Otherwise,
		 */
	} else {
		/**
		 * 2.1 Obtain an iterator from the map.
		 */
		std::map<int, GPIOPWMControlMapStruct>::iterator it = GPIOControlMap.begin();
		/**
		 * 2.1.1 Iterate over all of the items in the map.
		 */
		while (it != GPIOControlMap.end()) {
			/**
			 * 2.1.1.1 Determine whether the time matches the time that we turn the pin off.
			 */
			if ((it->second.dutyCycle == currentCount)) {
				/**
				 * 2.1.1.1.aThe count and changed flag are such that we should turn off the given pin.
				 */
				it->second.gpioPin->setValue(se3910RPi::GPIO::GPIO_LOW);
			}
			/**
			 * Increment the iterator.
			 */
			it++;

		}

	}

	/**
	 * 3. Increment the current count by 1, keeping it such that 0 <= currentCount < 100.
	 */
	currentCount = (currentCount + 1) % 25;
}

