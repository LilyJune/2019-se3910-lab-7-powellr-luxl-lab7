/**
 * @file CommandQueue.cpp.cpp
 * @author  Walter Schilling (schilling@msoe.edu)
 * @version 1.0
 *
 * @section LICENSE
 *
 *
 * This code is developed as part of the MSOE SE3910 Real Time Systems course,
 * but can be freely used by others.
 *
 * SE3910 Real Time Systems is a required course for students studying the
 * discipline of software engineering.
 *
 * This Software is provided under the License on an "AS IS" basis and
 * without warranties of any kind concerning the Software, including
 * without limitation merchantability, fitness for a particular purpose,
 * absence of defects or errors, accuracy, and non-infringement of
 * intellectual property rights other than copyright. This disclaimer
 * of warranty is an essential part of the License and a condition for
 * the grant of any rights to this Software.
 *
 * @section DESCRIPTION
 * Implementation of the AD Reader.
 * This class implements code to interface with an A/D reader.  The chip on the Alphabot 2 is the TLC1543: 10-bit AD acquisition chip.
 * The chip supports reading 11 different input channels and provides 10 bits of resolution.
 */
#include "ADReader.h"
#include "TaskRates.h"

/**
 * This macro defines a short pause, used to cause a simple inline delay in the code of no specific length.  Loop size can be varied if needed.
 */
#define SHORTPAUSE() {int ct; for (ct=0; ct < 100; ct+=2){ct++; ct--;}}

/**
 * This is the constructor for the A/D converter code.  It will instantiate a new object to read this material.
 * @param csPinNumber This is the GPIO pinb number for the chip select line.
 * @param clockPinNumber This is the GPIO pin number used for the clock.
 * @param addressPinNumber This is the GPIO pin number for the address line.
 * @param dinPinNumber This is the GPIO pin number used for reading in data.
 * @param threadName This is the name of the thread that will be running.
 * @param period  This is the period for the periodic task.
 */
ADReader::ADReader(int csPinNumber, int clockPinNumber, int addressPinNumber,
		int dinPinNumber, std::string threadName,
		uint32_t period) :
		PeriodicTask(threadName, period)
{
	/**
	 * Instantiate a new GPIO pin for the cs, setting it as output and defaulting to high.
	 */
	csIOPin = new GPIO(csPinNumber, GPIO::GPIO_OUT, GPIO::GPIO_HIGH);

	/**
	 * Instantiate a new clock pin as an output pin and initialize it to low.
	 */
	clockPin = new GPIO(clockPinNumber, GPIO::GPIO_OUT, GPIO::GPIO_LOW);

	/**
	 * Instantiate the address pin as an output with an initial value of low.
	 */
	addressIOPin = new GPIO(addressPinNumber, GPIO::GPIO_OUT, GPIO::GPIO_LOW);

	/**
	 * Inituialize the data out pin as an input pin.
	 */
	doutPin = new GPIO(dinPinNumber, GPIO::GPIO_IN);
}

/**
 * This is the deconstructor.  It will simply remove all allocated objects.
 */
ADReader::~ADReader() {

	delete csIOPin;
	delete addressIOPin;
	delete clockPin;
	delete doutPin;
}

/**
 * This method will read the a/d value from one of the channels.  The method is safe for multithreading.
 * @param channel This is the channel number that is to be read, between 0 and NUMBER_OF_AD_CHANNELS (exclusive).
 * @return The return will be the raw value between 0 and 1023 for the given channel.  It will return 0 if the value is out of range.
 */
uint16_t ADReader::readChannelValue(unsigned int channel) {
	uint16_t retValue = 0;
	if (channel < NUMBER_OF_AD_CHANNELS) {
		std::lock_guard<std::mutex> guard(dataMutex);
		// Obtain the correct item from the array.
		retValue = dataValues[channel];
	}
	return retValue;
}

/**
 * This method will read the max a/d value from one of the channels.  The method is safe for multithreading.
 * @param channel This is the channel number that is to be read, between 0 and NUMBER_OF_AD_CHANNELS (exclusive).
 * @return The return will be the raw value between 0 and 1023 for the given channel.  It will return 0 if the value is out of range.
 */
uint16_t ADReader::readChannelMax(unsigned int channel) {
	uint16_t retValue = 0;
	if (channel < NUMBER_OF_AD_CHANNELS) {
		std::lock_guard<std::mutex> guard(dataMutex);
		// Obtain the correct item from the array.
		retValue = maxDataValue[channel];
	}
	return retValue;

}

/**
 * This method will read the min a/d value from one of the channels.  The method is safe for multithreading.
 * @param channel This is the channel number that is to be read, between 0 and NUMBER_OF_AD_CHANNELS (exclusive).
 * @return The return will be the raw value between 0 and 1023 for the given channel.  It will return 0 if the value is out of range.
 */

uint16_t ADReader::readChannelMin(unsigned int channel) {
	uint16_t retValue = 0;
	if (channel < NUMBER_OF_AD_CHANNELS) {
		std::lock_guard<std::mutex> guard(dataMutex);
		// Obtain the correct item from the array.
		retValue = minDataValue[channel];
	}
	return retValue;
}


/**
 * This is the task method.  The task method will be invoked periodically every taskPeriod
 * units of time.
 */
void ADReader::taskMethod()

{
	/**
	 * Call the read sensors method.
	 */
	readSensors();
}

/**
 * This method will read the data from all 11 A/D channels.  The current values and max and min values will be updated.  AFter all 11 channels have been
 * read, the code will exit.
 */
void ADReader::readSensors() {
	/**
	 * This code is based upon the python code provided as sample code for this robot and the AD Senor.
	 */
	unsigned int currentChannel = 0;

	/**
	 * Iterate over the number of channels that are to be read.
	 */
	for (currentChannel = 0; currentChannel < NUMBER_OF_AD_CHANNELS;
			currentChannel++) {
		/**
		 * Set the CS low and pause momentarily.
		 */
		csIOPin->setValue(GPIO::GPIO_LOW);
		SHORTPAUSE();

		/**
		 *
		 * Write out the address and simultaneously read in the 10 data bits.  The 4 bit address goes first.
		 * Note that after the address is sent, 0's will be clocked for the rest of the values.
		 */
		unsigned int bitNumber;
		uint8_t tmp = ((currentChannel + 1) % NUMBER_OF_AD_CHANNELS) << 4;
		uint16_t data = 0;

		for (bitNumber = 0; bitNumber < 10; bitNumber++) {
			/**
			 *
			 *Send out the bits for the address.
			 */
			if (tmp & 0x80) {
				// Send out a 1 on the address bus.
				addressIOPin->setValue(GPIO::GPIO_HIGH);

			} else {
				// Send out a 0.
				addressIOPin->setValue(GPIO::GPIO_LOW);
			}
			/**
			 * Shift tmp 1 bit to the left.
			 */
			tmp = tmp << 1;

			/**
			 * Now read in the data bit.
			 */
			data = data << 1;

			/**
			 * If the bit is high, add it into the value.
			 */
			if (doutPin->getValue() == GPIO::GPIO_HIGH) {
				data |= 0x0001;
			}

			/**
			 * Set the clock high and pause for a moment.
			 */
			clockPin->setValue(GPIO::GPIO_HIGH);
			SHORTPAUSE();

			/**
			 * Set the clock pin low, causing a clock tick to occur.
			 */
			clockPin->setValue(GPIO::GPIO_LOW);
			SHORTPAUSE();
		}
		/**
		 * We have read in all 10 bits now.
		 */
		{
			/**
			 * Lock the mutex to protect / make everything thread safe.
			 */
			std::lock_guard<std::mutex> guard(dataMutex);
			/**
			 * Write the data in.
			 */
			dataValues[currentChannel] = data;
			/**
			 * Update the max and min reads as is applicable for the data entry.
			 */
			if (data < minDataValue[currentChannel]) {
				minDataValue[currentChannel] = data;
			}
			if (data > maxDataValue[currentChannel]) {
				maxDataValue[currentChannel] = data;
			}
		}
		/**
		 * Wait a bit for the next DA to complete.
		 * This takes at a minimum 100 microseconds.
		 */
		std::this_thread::sleep_for(std::chrono::microseconds(100));
	}
	/**
	 * Restore the chip select value.
	 */
	csIOPin->setValue(GPIO::GPIO_HIGH);
}

