package edu.msoe.sefocus.pcgui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class LabelEntryPanel extends JPanel  {
 private JLabel label;
 private JTextField entry;

    public LabelEntryPanel(String text, String initialValue)
    {
        label = new JLabel(text);
        entry = new JTextField(initialValue);

        this.setLayout((new BorderLayout()));
        this.add(label, BorderLayout.WEST);
        this.add(entry, BorderLayout.CENTER);
    }

    public void addActionListener(ActionListener l)
    {
        entry.addActionListener(l);
    }

    public String getEntryText()
    {
        return entry.getText();
    }

}
