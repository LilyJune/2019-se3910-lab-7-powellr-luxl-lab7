package edu.msoe.sefocus.pcgui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import edu.msoe.sefocus.core.iDiagnosticsController;
import edu.msoe.sefocus.core.iNavigationController;

/**
 * This class defines the controls which will adjust how the robot moves.
 *
 * @author schilling
 */
public class CalibratePanel extends JPanel {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private iNavigationController motorController;
    private iDiagnosticsController diagController;
    private final int FORWARD = 1;
    private final int BACKWARD = 2;
    private final int RIGHT = 3;
    private final int LEFT = 4;
    private final int STOP = 0;
    private int robotMotion = STOP;

    double ki=2.0, kp=50.0, kd=40.0;

    /**
     * This constructor will instantiate a new instance of the robot motion
     * controller, which will control the operation of the robot.
     *
     * @param pmctrl This is the instance of the robot propulsion motor controller that
     *               is to be controlled by this panel.
     */
    public CalibratePanel(iNavigationController navctrl, final iDiagnosticsController diagController) {
        motorController = navctrl;
        this.diagController = diagController;

        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());

        this.setLayout(new GridLayout(9, 1));

        JButton whiteCal = new JButton("Cal White");
        JButton blackCal = new JButton("Cal Black");
        JButton startLineFollow = new JButton("Start Follow");
        JButton stopLineFollow = new JButton("Stop Follow");
        JButton printDiagnostics = new JButton("Print Diagnostics");
        JButton resetDiagnostics = new JButton("Reset Diagnostics");

        LabelEntryPanel lkp = new LabelEntryPanel("kp", Double.toString(kp));
        LabelEntryPanel lkd = new LabelEntryPanel("kd", Double.toString(kd));
        LabelEntryPanel lki = new LabelEntryPanel("ki", Double.toString(ki));


        whiteCal.setAlignmentX(Component.CENTER_ALIGNMENT);
        blackCal.setAlignmentX(Component.CENTER_ALIGNMENT);
        startLineFollow.setAlignmentX(Component.CENTER_ALIGNMENT);
        stopLineFollow.setAlignmentX(Component.CENTER_ALIGNMENT);
        printDiagnostics.setAlignmentX(Component.CENTER_ALIGNMENT);
        resetDiagnostics.setAlignmentX(Component.CENTER_ALIGNMENT);
        lkp.setAlignmentX(Component.CENTER_ALIGNMENT);
        lki.setAlignmentX(Component.CENTER_ALIGNMENT);
        lkd.setAlignmentX(Component.CENTER_ALIGNMENT);

        this.add(whiteCal);
        this.add(blackCal);
        this.add(startLineFollow);
        this.add(stopLineFollow);
        this.add(printDiagnostics);
        this.add(resetDiagnostics);
        this.add(lkp);
        this.add(lkd);
        this.add(lki);

        whiteCal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                motorController.startCalibrationWhite();
            }
        });

        blackCal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                motorController.startCalibrationBlack();
            }
        });
        startLineFollow.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                motorController.startLineFollowing();
                navctrl.setKi(ki);
                navctrl.setKp(kp);
                navctrl.setKd(kd);
            }
        });

        stopLineFollow.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                motorController.stopLineFollowing();
            }
        });

        printDiagnostics.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                diagController.displayDiagnostics();
            }
        });

        resetDiagnostics.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                diagController.resetDiagnostics();
            }
        });

        lki.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String value = lki.getEntryText();
                    ki = Double.parseDouble(value);
                    navctrl.setKi(ki);
                } catch (NumberFormatException nfe) {
                    JOptionPane.showMessageDialog(null,
                            "The value " + lki.getEntryText() + " is not a valid double.",
                            "Invalid Value",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        lkp.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String value = lkp.getEntryText();
                    kp = Double.parseDouble(value);
                    navctrl.setKp(kp);
                } catch (NumberFormatException nfe) {
                    JOptionPane.showMessageDialog(null,
                            "The value " + lki.getEntryText() + " is not a valid double.",
                            "Invalid Value",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        lkd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String value = lkd.getEntryText();
                    kd = Double.parseDouble(value);
                    navctrl.setKd(kd);
                } catch (NumberFormatException nfe) {
                    JOptionPane.showMessageDialog(null,
                            "The value " + lki.getEntryText() + " is not a valid double.",
                            "Invalid Value",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        });


    }
}
